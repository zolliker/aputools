echo "Welcome to $HOSTNAME $(hostname -I)"
echo "$(cat /sys/class/net/enp1s0/address) .. $(cat /sys/class/net/enp4s0/address)"
export EDITOR=nano
function service_status () {
    for name in $@; do \
      echo ${name} $(systemctl is-active ${name}) $(systemctl is-enabled ${name} 2> /dev/null); \
      done | column -t | grep --color=always '\(disabled\|inactive\|$\)'
}
alias current='cat /root/aputools/current'
service_status router frappy display
echo "> current  # show currently installed state"

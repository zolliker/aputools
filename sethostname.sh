ETHNAME=$(cat /sys/class/net/enp1s0/address)
ETHNAME=${ETHNAME//:/}
APUID=${ETHNAME: -6}
FOUND="$(shopt -s nullglob; echo /root/aputools/servercfg/*_$APUID.cfg)"
if [ -z "$FOUND" ]; then
   HOSTNAME=apu$APUID
else
   FOUND=$(basename ${FOUND[0]})  # remove directory part
   HOSTNAME=${FOUND%%.*}  # remove extension
   if [ $HOSTNAME != "apu$APUID" ]; then
       HOSTNAME=${HOSTNAME%_*}
   fi
   echo "hostname $HOSTNAME"
fi
echo $HOSTNAME > /etc/hostname

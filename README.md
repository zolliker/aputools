# APU server

The APU is a Linux box to be used as a control box at LIN sample environment

servercfg/  contains the configuration files for all boxes

to_system/  contains the files to be installed in the system

## config files

filename: servercfg/(hostname)_(hexdigits).cfg

  * (hostname) the hostname to be given on startup
  * (hexdigits) the last six digits of the ethernet adress of the first interface on the APU (leftmost plug)

content of the config files:

```
[NETWORK]
enp1s0=192.168.127.254   # leftmost socket: connect here a moxa with factory settings
enp2s0=192.168.2.2       # connected device will get this ip via DHCP
enp3s0=192.168.3.3       #    or must be configured static to this IP
enp4s0=dhcp              # rightmost socket is the uplink

[ROUTER]
3000=/dev/ttyUSB0       # routing 3000 to the internal serial port /dev/ttyUSB0
5900=192.168.2.33       # routing VNC (local port 5900 to port 5900 on 192.168.2.33
8080=192.168.127.254:80 # routing 8080 to port 80 of 192.168.127.254

[FRAPPY]
cfg=uniax   # the cfg file for the frappy server
port=5000   # the port for the frappy server

[DISPLAY]
startup_text=startup...|HOST|ADDR   # startup text, 3 lines separated with |
```

## network configuration

The example above fits the most cases.
Remark: as the control boxes (MLZ type) are labelled with 'eth0' etc., the names might be given in this form.
Internally the more modern names like 'enp1s0' are used.

Here a detailed description of possible settings:

### fixed IP
```
enp2s0=192.168.3.5
```
Configure the IP address of the connected device to get the specified IP via DHCP or
configure the device to with this static IP. The last number must be within 1..254,
the APU port itself will get 1 (or 2, if the specified IP is 1).

### custom network
```
enp3s0=192.168.3.1/24  # our ip address, the static address of the connected device must
                       # be within 1-254 and not match our own
```
24 means 24 bit = 3 bytes network address.
A local network with static addresses. This way, the APU can get an other address than
192.168.x.1 or 192.168.x.2 or the network address size might be defined other than 24.
Probably needed rarely.

### uplink via DHCP
```
enp4s0=wan    # or enp4s0=dhcp
```
Uplink configured for DHCP in any network n.x.y.z with n < 192

### customized uplink
```
enp1s0=wan,192.168.1.0/24
```
Add more networks to the allowed uplink range. More than one range is allowed, comma separated.
Must not overlap networks specified for other ports!

### disabled port
```
enp3s0=off             # disabled
```

## display

available on control boxes (MLZ type) only. must therefore not be present on bare apu boxes.


## Installation of a Fresh Control Box or bare APU

### Tools / Resources needed

#### progress bar

On mac use ``brew install pv`` to install. If you do not want it, you may leve it out,
it is a matter of convenience. In this case omit ``| pv `` in the commands



## install image by means of the USB stick BOOT_TINY

The stick has TinyLinux on it and some additional scripts

If you do not have one, you may create it logging to an other box as root

### a) create a USB stick with TinyLinux (omit if you have the stick already)

to verify that /dev/sdb is your stick, use the following command before and after plugging the stick
```
apu> cd aputools
apu> bash mkusb.sh

... you are asked to give the device name from a list (typically sdb) ...

```

### b) mount with TinyLinux from USB stick

Connect a Mac with an USB to serial adapter (and null-modem!).
```
mac> screen /dev/tty.usbserial-130 115200
```

Do not yet connect LAN, plug USB stick and start the box, wait for prompt 'tc@box:'
```
apu> cd /media/BOOT_TINY  (or /media/CENTOS_APU)
apu> sh reg
```

enter new hostname (HWaddr  will be on the output)
register to PacketFence (if not yet done) and set role to sinq-samenv
connect LAN to rightmost socket on a bare APU or the leftmost socket an a control box
```
apu> sh tiny
...
some random art images are shown ...
...
inet addr:129.168.1.43  Bcast:192.168.1.255  Mask:255.255.255.0
inet addr:127.0.0.1  Mask:255.0.0.0
```

Use the displayed ip address for the following step.


### c) Copy Image to APU SSD

Note that the images are not part of the git repo.

```
mac> cd ~/switchdrive/apu/images
mac> dd if=apumaster.lz4 bs=512k | pv | ssh root@192.168.1.43 "lz4 -d | dd of=/dev/sda bs=512k"
```

(you are asked for root password, which is root, ``| pv `` may be omitted)

remove the stick, power off/on

login with root/FrappyLinse
```
> cd aputools
> git pull
> python3 install.py
...
enter host name:
...

> reboot now
```

DONE!


### d) Cloning an Image from an Existing Box

Use (b) above to boot from TinyLinux (replace the IP address by the one from the box!)
First make sure that a copy of the last apumaster.lz4 is saved with its date.
```
mac> ssh root@192.168.1.43 "dd if=/dev/sda bs=512k | lz4" | pv | dd of=apumaster.lz4 bs=512k
```


### e) Appendix

Source of scripts on TinyLinux:
```
apu> cat tini.sh
tce-load -wi nano
tce-load -wi lz4
tce-load -wi openssh
sudo /usr/local/etc/init.d/openssh start
sudo chpasswd << EOF
tc:tc
root:root
EOF
ifconfig | grep 'inet addr'
```



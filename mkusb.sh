# name of the USB stick to be created
NAME=BOOT_TINY

echo "enter ssh path of parent directory for boot_tiny, e.g. l_samenv@samenv:boxes"
read SRC

scp -r $SRC/boot_tiny ./

# list removeable disks
DISKS=()
while read LINE
do
  ARR=($LINE)
  if [[ ${ARR[@]:0:2} == "1 disk" ]]; then
    DISKS+=(${ARR[@]:2:1})
    printf "%-7s %7s " ${ARR[@]:2:2}
    echo ${ARR[@]:4}
  elif [[ ${ARR[@]:0:2} == "1 part" ]]; then
    printf "        %7s " ${ARR[@]:3:1}
    echo ${ARR[@]:4}
  fi
done < <(lsblk -l --output RM,TYPE,NAME,SIZE,LABEL,VENDOR,MODEL)

echo "which device? should be one of: ${DISKS[@]}"
read DEVICE

if [[ " ${DISKS[@]} " =~ " $DEVICE " ]]; then
  echo "create TinyLinux"

  if [[ $(cat /sys/block/$DEVICE/removable) != "1" ]]; then
    echo "/dev/$DEVICE is not a removable disk"
    exit
  fi

  DEVICE=/dev/$DEVICE

  umount ${DEVICE}1
  dd if=/dev/zero of=${DEVICE} count=1 conv=notrunc
  echo -e "o\nn\np\n1\n\n\nw" | fdisk ${DEVICE}
  mkfs.vfat -n $NAME -I ${DEVICE}1

  syslinux -i ${DEVICE}1
  dd conv=notrunc bs=440 count=1 if=boot_tiny/mbr.bin of=${DEVICE}
  parted ${DEVICE} set 1 boot on

  mkdir -p /mnt/apusb
  mount ${DEVICE}1 /mnt/apusb
  echo "copy files ..."
  cp boot_tiny/* /mnt/apusb/
  umount /mnt/apusb

  rm -rf boot_tiny
  echo "done."
else
  echo "/dev/$DEVICE is not a removeable disk"
fi

